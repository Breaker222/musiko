🡣 Documentation: English - Français

[![musiko](https://snapcraft.io//musiko/badge.svg)](https://snapcraft.io/musiko)

# ENGLISH

Musiko is a free to use, [open source](https://bitbucket.org/diatomee/musiko/src/master/) and cross platform **music player**.

## Download

* [Linux (snap)](https://snapcraft.io/musiko)
* [Other (dmg, exe, appImage)](https://bitbucket.org/diatomee/musiko/downloads/)

## Features

* Multiple audio formats (mp3, mp4, flac, wav, ogg, wma and more)
* Search and access by index
* Queue
* Playlists support

## Supported metadata

* year (release year)
* track (track number)
* disk (disc number)
* title (track title)
* album (album title)
* picture ("cover (front)" for album, other type for disc)
* synchronized lyrics (but used in one text)
* discsubtitle (disc title)
* albumartist (used as artwork name in Musiko)
* artist (for multiples artists, use "," as separator)
* genre (for multiples genres, use "," as separator)

Use ID3v2.3 or more be able to use all these tags. [Kid3](https://kid3.sourceforge.io/) is a good tag editor. If you wish more supported metadata, please [create issue](https://bitbucket.org/diatomee/musiko/issues/new) and explain your needs.

## Use

Musiko has a fairly simple interface to use. The software is still being improved, some points on its use deserve a few lines.

### Actions

**Left click** is defined as the main click. It allows to put in queue, disc or track. When hovering over the tracks in the waiting folder or in the selection panel, they become crossed out. This indicates that the left click will be used to remove the track from the file or selection. It is also used to navigate the software.

**Right click** is defined as secondary click. It lets you know the content of an album so you can see the discs, tracks and all the other information contained therein. Right-clicking on a track displays detailed information for that track.

**Middle click** (or Alt/Option + left click combination) allows you to select the tracks of an album, the tracks of a disc or even a single track. Select brings up the selection panel in which you can create compilations (or playlists).

In a future version, it will be possible to perform more actions through a context menu and define the actions according to the click.

### Playlists

Currently, it is simply possible to create a playlist from Musiko (with the selection, as explained in the "Actions" section. To modify or delete a playlist, go to the "play.lists" file located at the root of the music folder, indicated in the settings panel, then tweak a little while preserving the syntax faithfully When creating via Musiko, the image is inserted at the root of the music folder.

Here is an example of the content of the "play.lists" file:

```
#Chanteuses japonaises
>/chanteusesjaponaises.jpg
/Yoshiko Sai/Taiji no yume (1977)/10-胎児の夢.mp3
/Miharu Koshi/Chanson solaire (1995)/12-Parlez-moi d'amour.mp3
/Hako Yamasaki/Tobimasu/07 See No Shadows_影が見えない [Kage Ga Mienai].mp3
/Junko Kudo - Akane Iro No Carnival/8. すみれ・チューリップ.mp3
#Mix classique
>/monmix.png
/Debussy/La mer.flac
/Autre/Amarilli.mp3
/Camille de St Saens/danse-macabre.mp3
```

A playlist consists, in order, of a **title** (introduced by #), a **link to a cover** (introduced by >), then the list of **links to tracks**. The example above has two playlists. The first includes 4 tracks and the second includes 3 tracks.

In a future version, everything can be done from Musiko.

### Add lyrics

The lyrics are added to the choice, in:

* music metadata ("synchronized lyrics" field, although they will not be synchronized);
* a file with the name of the music, with the extension `.lyrics` to be inserted into the music folder or sub-folder. If the file is named `03. my_track.mp3`, the associated lyrics file is named `03. my_track.lyrics`.

If the lyrics are in both the metadata and in a file, the lyrics in the file will be used.

In a future version, everything can be done from Musiko.

## Technical (for developer)

Musiko uses Javascript, Electron, VueJS, the music-metadata module and a few others for its development. It organizes the tracks of a folder and its sub-folders into albums. Here is the structure of an album:

```
{
	album,
	artwork,
	cover,
	discs: [{
		disc,
		disccover,
		disctitle,
		tracks: [{
			artists: [],
			duration,
			genres: [],
			path,
			title,
			track,
			year,
			lyrics
		},...]
	},...]
}
```

---

# FRANÇAIS

Musiko est un **lecteur de musique** gratuit, [open source](https://bitbucket.org/diatomee/musiko/src/master/) et multi-OS.

## Téléchargements

* [Linux (snap)](https://snapcraft.io/musiko)
* [Autres (dmg, exe, appImage)](https://bitbucket.org/diatomee/musiko/downloads/) 

## Fonctionnalités

* Grand nombre de formats audio supportés (mp3, mp4, flac, wav, ogg, wma et plus)
* Recherche et accès par index
* File d'attente
* Création de compilations (playlists)

## Métadonnées supportées

* année (année de sortie)
* piste (numéro de piste)
* disque (numéro de disque)
* titre (titre de la piste)
* album (titre de l'album)
* image ("pochette (avant)" pour un album, autre type pour un disque)
* paroles synchronisée (utilisées en non-synchronisée dans Musiko)
* sous-titre de disque (titre du disque)
* artiste de l'album (utilisé comme nom d'oeuvre dans Musiko)
* artiste (pour plusieurs artistes, utiliser "," comme séparateur)
* genre (pour plusieurs genres, utiliser "," comme séparateur)

Il faut utiliser ID3v2.3 (ou version supérieure) afin d'accéder à toutes ces métadonnées. [Kid3](https://kid3.sourceforge.io/) est un bon éditeur de métadonnées.

## Utilisation

Musiko dispose d'une interface assez simple à prendre en main. Le logiciel étant toujours en cours d'amélioration, certains points sur son utilisation méritent quelques lignes.

### Actions

Le **clic gauche** est défini comme étant le clic principal. Il permet de mettre en file d'attente album, disque ou piste. Au survol des pistes dans la file d'attente ou dans le panneau de sélection, celles-ci deviennent barrées. Cela indique que le clic gauche sera utilisé pour retirer la piste de la file ou de la sélection. Il sert également à naviguer dans le logiciel.

Le **clic droit** est défini comme étant le clic secondaire. Il permet de connaître le contenu d'un album pour ainsi voir les disques, les pistes et toutes les autres informations qu'il contient. Un clic droit sur une piste affiche les informations détaillées de cette piste.

Le **clic du milieu** (ou la combinaison Alt/Option + clic gauche) permet de sélectionner les pistes d'un album, les pistes d'un disque ou encore, une piste unique. Sélectionner fait apparaître le panneau de sélection dans lequel on peut créer des compilations (ou playlists).

Lors d'une future version, il sera possible d'effectuer plus d'actions grâce à un menu contextuel et de définir les actions en fonction du clic.

### Compilations (playlists)

Actuellement, il est simplement possible de créer une compilation depuis Musiko (avec la sélection, comme expliqué dans la section "Actions". Pour modifier ou supprimer une compilation, il faut se rendre dans le fichier "play.lists" qui se situe à la racine du dossier des musiques (indiqué dans le panneau des paramètres), puis bidouiller un peu en préservant fidèlement la syntaxe. Lors de la création via Musiko, l'image s'insère à la racine du dossier des musiques.

Voici un exemple de contenu du fichier "play.lists" :

```
#Chanteuses japonaises
>/chanteusesjaponaises.jpg
/Yoshiko Sai/Taiji no yume (1977)/10-胎児の夢.mp3
/Miharu Koshi/Chanson solaire (1995)/12-Parlez-moi d'amour.mp3
/Hako Yamasaki/Tobimasu/07 See No Shadows_影が見えない [Kage Ga Mienai].mp3
/Junko Kudo - Akane Iro No Carnival/8. すみれ・チューリップ.mp3
#Mix classique
>/monmix.png
/Debussy/La mer.flac
/Autre/Amarilli.mp3
/Camille de St Saens/danse-macabre.mp3
```

Une liste de lecture se compose, dans l'ordre, d'un **titre** (introduit par #), d'un **lien vers une pochette** (introduit par >), puis de la liste des **liens vers les musiques**. L'exemple ci-dessus comporte deux listes de lecture. La première comprend 4 musiques et la seconde en comprend 3.

Lors d'une future version, tout pourra se faire depuis Musiko.

### Ajout de paroles

Les paroles s'ajoutent au choix, dans :

* les métadonnées de la musique (champ "paroles synchronisée", bien qu'elles ne seront pas synchronisées);
* un fichier portant le nom de la musique, avec l'extension `.lyrics` à insérer dans le dossier ou le sous-dossier de la musique. Si le fichier se nomme `03. ma_piste.mp3`, le fichier de paroles associées se nomme `03. ma_piste.lyrics`.

Si les paroles se trouvent à la fois dans les métadonnées et dans un fichier, celles du fichier seront utilisées.

Lors d'une future version, les paroles seront insérables et modifiables depuis Musiko.

## Technique (pour développeur)

Musiko utilise pour son développement Javascript, Electron, VueJS, le module music-metadata et quelques autres. Il organise les pistes d'un dossier et de ses sous-dossier en albums. Voici la structure d'un album :

```
{
	album,
	artwork,
	cover,
	discs: [{
		disc,
		disccover,
		disctitle,
		tracks: [{
			artists: [],
			duration,
			genres: [],
			path,
			title,
			track,
			year,
			lyrics
		},...]
	},...]
}
```

