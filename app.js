const
	{app} = require('electron').remote,
	{ipcRenderer, shell, remote, BrowserWindow} = require('electron'),
	path = require('path'),
	fs = require('fs'),
	BASELINE = '<em>La muziko estas grandioza.</em>',
	savePATH = path.join(app.getPath('appData'), 'musiko'),
	VERSION = app.getVersion(), // version in package.json
	isLinux = process.platform === 'linuxgood',
	currentWindow = remote.getCurrentWindow()

//Lancement de l'application. Récupère pour cela les JSONs dans savePATH
function launchApp() {
	function getContent(filename, defaultReturn = []) {
		const p = path.join(savePATH, filename + '.json')
		if (fs.existsSync(p)) return JSON.parse(fs.readFileSync(p))
		return defaultReturn
	}
	const
		albums = getContent('collection'),
		/*lists = getContent('playlists').map(l => ({
			album: l.n,
			cover: l.cover,
			artwork: '❤',
			discs: [{
				disc: 0,
				tracks: l.t.map(arrADT => { //une piste est une réf à un num d'Album, à un num Disc et à un num Track
					let a = arrADT[0], d = arrADT[1], t = arrADT[2]
					a = albums[a]
					d = a.discs[d]
					t = d.tracks[t]
					const cloneT = { ...t } //façon de cloner un objet (spread)
					cloneT.albumRef = a
					cloneT.discRef = d
					return cloneT
				})
			}]
		})),*/
		genres = getContent('genres'),
		years = getContent('years'),
		lastAlbums = getContent('lastAlbums'),
		config = getContent('config', {})
	
	document.body.setAttribute('data-theme', config.theme)
	let a = {
		configPath: savePATH,
		language: config.language,
		theme: config.theme || 'dark',
		click1: 1, //config.click1 || 1, //clic gauche: ajouter à la file d'attente
		click2: 3, //config.click2 || 3, //clic droit: voir les infos
		click3: 5, //config.click3 || 5, //clic milieu: sélectionner
		L: langObj(config.language),
		currentForTriple: 'Album',
		albums, lists: [], genres, years,
		lastAlbums,
		nbLastAlbums: config.nbLastAlbums,
		collectionPath: config.collectionPath,
		tracks: [],
		queue: [], past: [], sel: [],

		//donné par getAlbumsAndTracksOf(value, of) quand demandé
		byGenre: {albums: [], tracks: []},
		byYear: {albums: [], tracks: []},
		byArtist: {albums: [], tracks: []},

		defaultTitle: BASELINE,
		currentTitle: BASELINE,
		showPause: false,
		audio: undefined,
		inPan: undefined, //music in left panel {album, track, artist: []}
		currentQPlayed: undefined,
		getAlbumsAndTracksOf: function (value, of) {
			let byAlbum = [], byTrack = []
			for (const album of this.albums) {
				for (const d of album.discs) {
					for (const track of d.tracks) {
						let condition
						if (of === 'genre') {
							condition = track.genres && track.genres.indexOf(value) !== -1
						} else if (of === 'year') {
							condition = track.year && track.year === value
						} else if (of === 'artist') {
							condition = track.artists && track.artists.indexOf(value) !== -1
						}
						if (condition) byTrack.push({track, album})
					}
				}
			}
			// Réunion des pistes ayant le même album et le même artwork.
			for (const t of byTrack) {
				const i = byAlbum.findIndex(el => el.artworkName === t.album.artwork && el.albumName === t.album.album)
				if (i === -1) {
					// on ajoute un objet à byAlbum
					byAlbum.push({
						artworkName: t.album.artwork,
						albumName: t.album.album,
						tracks: [t]
					})
				} else {
					// on ajoute la piste à l'index
					byAlbum[i].tracks.push(t)
				}
			}
			// Ensuite on récupère le nombre de piste d'un album et on le compare au nombre de piste dans byAlbum. Si le compte y est, on met dans albums. S'il n'y est pas, on met dans tracks.
			let albums = [], tracks = []
			for (const a1 of byAlbum) {
				for (const a of this.albums) {
					if (a1.artworkName === a.artwork && a1.albumName === a.album) {
						let nbTracks = 0
						for (const d of a.discs) nbTracks += d.tracks.length
						if (a1.tracks.length === nbTracks) {
							albums.push(a1.tracks[0].album)
						} else {
							for (const t of a1.tracks) tracks.push(t)
						}
						continue
					}
				}
			}
			// Note : on pourrait améliorer cette fonction vu que tout est trié alphabétiquement. Si la lettre est après ce que l'on recherche, on passe à l'élément suivant.
			const obj = {albums, tracks}
			if (of === 'genre') this.byGenre = obj
			if (of === 'year') this.byYear = obj
			if (of === 'artist') this.byArtist = obj
			return obj
		},
		getCtx: function(e) {
			if (document.getElementById('ctx')) document.body.removeChild(document.getElementById('ctx'))
			const box = document.createElement('div')
			box.id = 'ctx'
			box.classList.add('ctx')
			box.innerHTML = `
				<div class=item>Mettre en attente</div>
				<div class=item>Jouer</div>
				<hr>
				<div class=item>Voir les infos</div>
				<div class=item>Afficher les paroles</div>
			`
			const wH = window.innerHeight/2
			const wW = window.innerWidth/2
			const X = e.clientX
			const Y = e.clientY
			let l = 'auto', b = 'auto', r = 'auto', t = 'auto'
			if(Y > wH && X <= wW) {
				l = X + 'px'
				b = window.innerHeight - Y + 'px'
			} else if(Y > wH && X > wW) {
				b = window.innerHeight - Y + 'px'
				r = window.innerWidth - X + 'px'
			} else if(Y <= wH && X <= wW) {
				l = X + 'px'
				t = Y + 'px'
			} else {
				r = window.innerWidth - X + 'px'
				t = Y + 'px'
			}
			box.style.left = l
			box.style.bottom = b
			box.style.right = r
			box.style.top = t
	
			document.body.appendChild(box)
		},
		shuffle: arr => arr.map(a => [Math.random(), a]).sort((a, b) => a[0] - b[0]).map(a => a[1]),
		getTime: function (t) {
			let
				h = Math.floor(t / 3600),
				m  = Math.floor((t % 3600) / 60),
				s  = Math.floor(t % 60)
			if (s < 10) s = '0' + s
			if (m < 10) m = '0' + m
			const res = (h ? h + ':' : '') + m + ':' + s
			if (res === 'NaN:NaN') return '-:-'
			return res
		},
		Sound: function () {
			if (this.queue.length === 0) {
				this.currentTitle = this.defaultTitle
				document.title = this.currentTitle === this.defaultTitle ? 'Musiko' : this.currentTitle
				this.audio = undefined
				this.showPause = false
				setTimeout(() => document.getElementById('timeline').style.width = '0px', 100)
				return
			}
			const t = this.queue[0].track
			this.audio = new Audio()
			this.audio.src = this.collectionPath + t.path
			this.audio.load()
			this.currentTitle = t.title +  ' — ' + t.artists.join(', ')
			this.inPan = this.queue[0]
			this.currentQPlayed = this.queue[0]
	
			//sauvegarde de l'album des dernières écoutes
			const newAlbum = this.currentQPlayed.album
			let la = this.lastAlbums.filter(
				alb => alb.album !== newAlbum.album && alb.artwork !== newAlbum.artwork
			)
			la.unshift(newAlbum)
			la = la.slice(0, this.nbLastAlbums)
			this.lastAlbums = la
			fs.writeFileSync(path.join(savePATH, 'lastAlbums.json'), JSON.stringify(this.lastAlbums))
	
			document.title = this.currentTitle
			this.past.unshift(this.queue.shift())
	
			this.showPause = true
			this.audio.play()
			this.audio.addEventListener('ended', function() {
				a.audio = undefined
				a.Sound()
			}, false)
			this.audio.addEventListener('timeupdate', function() {
				//this = audio
				if (!this) return
				let t = a.getTime(this.duration - this.currentTime)
				if (t === 'NaN:NaN') return
				if (a.progressValue !== t) {
					document.title = a.currentTitle + ' (' + t + ')'
					a.progressValue = t
				}
				const w = `calc(100% * ${this.currentTime} / ${this.duration})`
				document.getElementById('timeline').style.width = w
			}, false)
		},
		playAlbum: function(album, discIndex) {
			if (discIndex !== false) {
				for (const d of album.discs) for (const track of d.tracks) {
					this.queue.push({track, album})
				}
			} else {
				for (const track of album.discs[discIndex].tracks) {
					this.queue.push({track, album})
				}
			}
			if (!this.audio) this.Sound()
		},
		playAlbumDirect: function(album, discIndex) {
			if (discIndex !== false) {
				for (let d of album.discs) for (let track of d.tracks) {
					this.queue.unshift({track, album}) //ajoute dans l'ordre inverse
				}
			} else {
				for (let track of album.discs[discIndex].tracks) {
					this.queue.unshift({track, album}) //ajoute dans l'ordre inverse
				}
			}
			this.skip()
		},
		selectAlbum: function(album, discIndex) {
			if (discIndex !== false) {
				for (const d of album.discs) for (const track of d.tracks) {
					this.sel.push({track, album})
				}
			} else {
				for (const track of album.discs[discIndex].tracks) {
					this.sel.push({track, album})
				}
			}
		},
		selectArtists: function(artists) {
			for (const artist of artists) {
				const {albums, tracks} = this.getAlbumsAndTracksOf(artist, 'artist')
				for (const album of albums) {
					for (const d of album.discs) {
						for (const track of d.tracks) {
							this.sel.push({track, album})
						}
					}
				}
				for (const t of tracks) {
					this.sel.push({track: t.track, album: t.album})
				}
			}
		},
		playTrack: function(q) {
			this.queue.push(q)
			if (!this.audio) this.Sound()
		},
		playTrackDirect: function(q) {
			this.queue.unshift(q)
			this.skip()
		},
		selectTrack: function(track, album) {
			this.sel.push({track, album})
		},
		playPause: function () {
			if (!this.audio && this.queue.length === 0) {
				// joue un album parmi ceux affichés, choisi aléatoirement
				const albums = this.albums
				const album = albums[Math.floor(Math.random() * albums.length)]
				for (let disc of album.discs) for (let track of disc.tracks) {
					this.queue.push({track, album})
				}
				// joue la première musique de queue
				this.Sound()
				return
			}
			if (!this.audio.paused) {
				// met en pause la lecture
				this.audio.pause()
				this.showPause = false
				return
			}
			// reprend la lecture
			this.audio.play()
			this.showPause = true
		},
		stop: function () {
			if (this.audio) {
				this.audio.pause()
				this.audio.currentTime = 0
				this.showPause = false
			}
		},
		random: function () {
			if (this.queue.length === 0) {
				// crée une queue de 50 musiques à partir de tous les albums
				for (let a of this.albums) for (let d of a.discs) for (let t of d.tracks) {
					this.queue.push({track: t, album: a})
				}
				this.queue = this.shuffle(this.queue).slice(0, 50)
				if (!this.audio) {
					// crée l'audio et le joue
					this.Sound()
					return
				}
				return
			}
			// mélange la queue existante
			this.queue = this.shuffle(this.queue)
		},
		skip: function() {
			if (this.audio) {
				this.audio.pause()
				this.Sound()
			}
		}
	}
	if (a.albums.length !== 0) {
		const rdmAlbum = a.shuffle(a.albums)[0]
		const rdmDisk = a.shuffle(rdmAlbum.discs)[0]
		const rdmTrack = a.shuffle(rdmDisk.tracks)[0]
		a.inPan = {track: rdmTrack, album: rdmAlbum}
	}
	const vm = new Vue({
		el: "#app",
		components: {Banner, Asidepan, Album},
		data: {
			a,
			I: icons,
			tab: "Album",
		},
		methods: {
			getIndexs: function () {
				let chars = []
				for (const album of a.albums) {
					const c = album.artwork.split('')[0].toUpperCase()
					if (chars.indexOf(c) === -1) chars.push(c)
				}
				return chars
			},
			goTo: function (char) {
				const elts = document.body.querySelectorAll("[data-name='artwork']")
				let go = false
				for (const e of [... elts]) {
					if (e.textContent.split('')[0].toUpperCase() === char) {
						if (!go) {
							e.parentNode.parentNode.scrollIntoView()
							go = true
						}
						e.classList.add('blink')
						const i = setInterval(() => {
							e.classList.remove('blink')
							clearInterval(i)
						}, 800)
					}
				}
			}
		}
	})
	return vm
}

// INIT : Cette fonction génère la collection en prenant en compte une ancienne config si elle est dispo. À la fin de la génération, on obtient plusieurs JSONs dans savePATH et l'app est relancée (ce qui ait qu'on revient au routage).
function initCollection(configRecup) {
	let config, isNewVersion = false
	function getLang() {
		const l = ['fr', 'de', 'it', 'ae', 'ru', 'es', 'jp', 'cn']
		const i = l.indexOf(app.getLocale().slice(0, 2))
		return i > -1 ? l[i] : 'en'
	}
	if (!configRecup && fs.existsSync(path.join(savePATH, 'config.json'))) {
		const {collectionPath, language, theme, nbLastAlbums} = JSON.parse(fs.readFileSync(path.join(savePATH, 'config.json')))
		config = {collectionPath, language, theme, nbLastAlbums}
	} else if (configRecup) {
		config = configRecup
		isNewVersion = configRecup.version !== VERSION
	} else {
		config = {
			collectionPath: app.getPath('music'),
			nbLastAlbums: 9,
			language: getLang(),
			theme: 'dark',
			click1: 1,
			click2: 3,
			click3: 5
		}
	}
	const note = `
		<h1>Updated to Musiko ${VERSION}</h1>
		<ol>
			<li>New panel: Mixing (regroups for the moment the playlists and the last plays)</li>
			<li>Creation of playlists from Musiko</li>
			<li>No need to refresh the collection to update the playlists</li>
			<li>Merger of the Album, Track and Artist panels</li>
			<li>Move the search for works by index (bottom right, on hover)</li>
			<li>Improved actions + selection (to create a playlists)</li>
			<li>Updated documentation</li>
		</ol>
		<p><em>The release notes are accessible via the Settings panel, by clicking on the current version number.</em></p>
		<div class=bt id=ok>OK</div>
	`
	config.version = VERSION
	document.body.setAttribute('data-theme', config.theme)
	const L = langObj(config.language)
	const splashload = document.createElement('div')
	splashload.classList.add('splashload')
	splashload.innerHTML = L.creationCollection + `<div id=loader></div>`
	if (isNewVersion) splashload.innerHTML += `<div class=versionNote id=vn>${note}</div>`
	document.body.appendChild(splashload)
	const loader = document.getElementById('loader')
	if (isNewVersion) {
		document.getElementById('ok').addEventListener('click', () => {
			isNewVersion = false
			splashload.removeChild(document.getElementById('vn'))
		})
	}
	ipcRenderer.on('info' , (e, data) => {
		loader.innerHTML = data.nb + ' / ' + data.total + ' ' + L.musiquesAnalysees
	})
	ipcRenderer.on('finish', (e, data) => {
		if (isNewVersion && document.getElementById('ok')) {
			//c'est une nouvelle version. L'app ne se relancera que si on clique sur OK.
			document.getElementById('ok').addEventListener('click', () => currentWindow.reload())
		} else currentWindow.reload()
	})
	ipcRenderer.send('initCollection', config, savePATH)
}

// ROUTAGE : Si on a un config.json, on lance l'app normalement, sinon on initialise la collection
let vm
if (fs.existsSync(path.join(savePATH, 'config.json'))) {
	const configRecup = JSON.parse(fs.readFileSync(path.join(savePATH, 'config.json')))
	if (configRecup.version !== VERSION) { // Maj détectée
		fs.rmdirSync(savePATH, { recursive: true })
		initCollection(configRecup)
		//vm = launchApp()
	} else {
		vm = launchApp()
	}
} else initCollection()

// ROUTAGE DE LA LANGUE
function langObj(lang) {
	if (lang === 'ae') return ae
	if (lang === 'cn') return cn
	if (lang === 'de') return de
	if (lang === 'en') return en
	if (lang === 'eo') return eo
	if (lang === 'es') return es
	if (lang === 'fr') return fr
	if (lang === 'it') return it
	if (lang === 'jp') return jp
	if (lang === 'ru') return ru
}

// Récup des playlists
ipcRenderer.on('receiveLists', (e, data, app) => {
	console.log('ipc: réception des infos')
	vm.a.lists = data.lists
	console.log(data)
	if (data.logSyntax.length > 0 || data.logPath.length > 0) {
		const logListsPath = path.join(app.collectionPath, 'play.lists.log')
		alert(`Error(s) in play.lists file. Show "${logListsPath}" for more details.`)
	}
})

// SAISIE AU CLAVIER : ouverture du panneau Search
document.addEventListener('keydown', function (e) {
	if (e.which === 112) {
		window.open('https://bitbucket.org/diatomee/musiko/src/master/README.md', '_blank', 'nodeIntegration=no')
	} else if ([8,9,13,17,18,20,27,37,38,40,45,46,113,114,115,116,117,118,119,120,121,122,123,225].indexOf(e.which) !== -1) { //touches qui ne sont ni chiffre, ni lettre, ni ponctuation
		//on ne fait rien
	}else {
		if (e.target.tagName === 'BODY' && vm) {
			console.log(e.which)
			vm.tab = 'Search'
		}
	}
})