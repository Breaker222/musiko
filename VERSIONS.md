# Versions

## 1.0.7 (future)

**EN**

1. New panel: Mixing (regroups for the moment the compilations and the last plays).
2. Creation of compilations (playlists) from Musiko.
3. No need to refresh the collection to update the compilations.
4. Merger of the Album, Track and Artist panels.
5. Move the search for works by index (bottom right, on hover).
6. Improved actions + selection (to create a compilation).
7. Updated documentation.

**FR**

1. Nouveau panneau : Mélange (regroupe pour le moment les compilations et les dernières écoutes).
+2. Création des compilations (playlists) depuis Musiko.
3. Plus besoin de rafraîchir la collection pour mettre à jour les compilations.
4. Fusion des panneaux Album, Piste et Artiste.
5. Déplacement de la recherche d'oeuvres par index (en bas à droite, au survol).
+6. Actions améliorées + sélection (pour créer une compilation).
7. Documentation mise à jour.


Édition des métadonnées dans un fichier texte :

Id - Id
Titre - Title
Artistes - Artists
Genres - Genres
Année - Year
Mots-clés - Keywords
Références - References
Album



## 1.0.6 (current / actuelle)

**EN**

1. Choice of 10 themes to change the look.
2. New panel: Artists.
3. Improved research.
4. Panel removal: Filters.
5. Various bugs fixed.
6. Adding this small update window ;)

**FR**

1. Choix parmi 10 thèmes pour changer l'apparence.
2. Nouveau panneau : Artistes.
3. Amélioration de la recherche.
4. Suppression du panneau : Filtres.
5. Divers bogues corrigés.
6. Ajout de cette petite fenêtre de mise à jour ;)

## 1.0.5

**FR**

1. La création de la collection musicale est plus rapide et se fait jusqu'au bout.
2. Si la langue de l'utilisateur est indisponible, l'anglais sera mis par défaut.
3. Parmi plusieurs pochettes disponibles dans le dossier ou le sous-dossier où se trouve la musique, le choix se portera prioritairement sur :
	1. cover.[ext]
	2. folder.[ext]
	3. front.[ext]
	4. [max-file-size].[ext]
4. Lors d'une mise à jour, la collection n'est plus rafraîchit systématiquement.
5. Recherche améliorée :
	- saisir ":" + lettre = accéder à l'index dans la liste des albums
	- commencer à saisir du texte n'importe où pour débuter une recherche
	- les résultats de recherche sont cliquables directement pour être joués ou observés.
