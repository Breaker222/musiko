const de = {
    creationCollection: 'Erstellung der Musiksammlung.',
    musiquesAnalysees: 'Musik analysiert.',
    pasAlbum: 'Kein Album gefunden. Der Pfad zum Musikalische Aufzeichnung kann in den Einstellungen geändert werden.',
    //panneau recherche
    oeuvreParIndex: 'Arbeit nach Index',
    rechercheGlobale: 'Globale Suche',
    resultats: 'Ergebnisse :',
    recherche: 'Name des Albums, der Arbeit, des Künstlers oder des Titels.',
    //panneau album
    disque: 'Disc',
    //panneau file d'attente
    fileAttenteVide: 'Die Warteschlange ist leer.',
    vider: 'Leeren Sie',
    historiqueVide: 'Die Geschichte ist leer.',
    pisteAttente: (nb, time) => `${nb} Track${nb > 1 ? 's' : ''} warren (${time}).`,
    pisteLancee: nb => nb === 1 ? 'Ein Track wurde bereits gestartet.' : nb + ' Tracks bereits gestartet.',
    //panneau filtres
    genres: 'Musikgenres',
    annees: 'Jahre',
    retirerFiltres: 'Entfernen Sie alle Filter',
    //panneau params
    dossierMusical: 'Musikalische Aufzeichnung',
    dossierConfig: 'Konfigurationsordner',
    choisirDossier: 'Wählen Sie einen Aufzeichnung',
    informations: 'Informationen',
    version: 'Version',
    aide: 'Dokumentation',
    language: 'Sprache',
    theme: 'Aussehen',
    nbAlbums: 'Anzahl der Alben vom letzten Hören',
    action: 'Aktionen',
    actions: '<b>Linksklick</b>: Abspielen, Halten oder Spezialaktion<br><b>Mittelklick oder Strg + Klick</b>: Wählen Sie (nützlich zum Erstellen einer Zusammenstellung)<br><b>Rechtsklick</b>: Weitere Informationen anzeigen',
    //panneau sel
    exempleCompil: 'Meine Playlist',
    creerCompil: 'Erstellen Sie eine Wiedergabeliste',
    creer: 'Erstellen',
    choisirCouv: 'Wählen Sie ein Cover oder ziehen Sie es hierher',
    selection: 'Auswahl',
    erreurImg: 'Bildname bereits vorhanden. Sie müssen es ändern (direkt unter dem Bild).',
    //panneau mixer
    compilations: 'Wiedergabelisten',
    dernieresEcoutes: 'Letzte Spiele'
}