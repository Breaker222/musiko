const eo = {
    creationCollection: 'Kreo de la muzika kolekto.',
    musiquesAnalysees: 'muziko analizita.',
    pasAlbum: 'Neniu albumo trovita. La vojo al la muzika dosierujo povas esti ŝanĝita en la agordoj.',
    //panneau recherche
    oeuvreParIndex: 'Labori per indekso',
    rechercheGlobale: 'Serĉu ĉiujn',
    resultats: 'Rezultoj :',
    recherche: 'Albumo, verko, artisto aŭ titola nomo.',
    //panneau album
    disque: 'Disko',
    //panneau file d'attente
    fileAttenteVide: 'La vosto estas malplena.',
    vider: 'Malplenigi',
    historiqueVide: 'La historio estas malplena.',
    pisteAttente: (nb, time) => `${nb} trako${nb > 1 ? 'j' : ''} pritraktata (${time}).`,
    pisteLancee: nb => nb === 1 ? 'Trako jam lanĉita.' : nb + ' trakoj jam lanĉitaj.',
    //panneau filtres
    genres: 'Ĝenroj',
    annees: 'Jaroj',
    retirerFiltres: 'Forigu ĉiujn filtrilojn',
    //panneau params
    dossierMusical: 'Muzika dosierujo',
    dossierConfig: 'Agordo-dosierujo',
    choisirDossier: 'elektu dosierujon',
    informations: 'Novaĵoj',
    version: 'versio',
    aide: 'Dokumentado',
    language: 'Lingvo',
    theme: 'Aspekto',
    nbAlbums: 'Nombro de albumoj de la lastaj aŭskultiloj',
    action: 'Agoj',
    actions: '<b>Maldekstra alklako</b>: Ludi, malhelpi aŭ specialan agon<br><b>Meza klako aŭ Alt/Option + alklaki</b>: Elektu (utila por krei kompilaĵon)<br><b>Dekstra alklako</b>: Vidi pliajn informojn',
    //panneau sel
    exempleCompil: 'Mia playlist',
    creerCompil: 'Krei playlist',
    creer: 'Krei',
    choisirCouv: 'Elektu kovrilon aŭ trenu ĝin ĉi tie',
    selection: 'Selektado',
    erreurImg: 'Bildnomo jam ĉeestanta. Vi devas ŝanĝi ĝin (ĝuste sub la bildo).',
    //panneau mixer
    compilations: 'Ludlistoj',
    dernieresEcoutes: 'Lastaj teatraĵoj'
}