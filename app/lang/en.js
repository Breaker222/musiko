en = {
    creationCollection: 'Building music collection.',
    musiquesAnalysees: 'tracks analyzed.',
    pasAlbum: 'No album found. The path to the music folder can be changed in the settings.',
    //panneau recherche
    oeuvreParIndex: 'Work by index',
    rechercheGlobale: 'Global search',
    resultats: 'Results:',
    recherche: 'Album, work, artist or track title name.',
    //panneau album
    disque: 'Disk',
    //panneau file d'attente
    fileAttenteVide: 'The queue is empty.',
    vider: 'Empty',
    historiqueVide: 'The history is empty.',
    pisteAttente: (nb, time) => `${nb} track${nb > 1 ? 's' : ''} pending (${time}).`,
    pisteLancee: nb => nb === 1 ? 'A track already launched.' : nb + ' tracks already launched.',
    //panneau filtres
    genres: 'Genres',
    annees: 'Years',
    retirerFiltres: 'Remove all filters',
    //panneau params
    dossierMusical: 'Music folder',
    dossierConfig: 'Configuration folder',
    choisirDossier: 'Choose a folder',
    informations: 'Informations',
    version: 'Version',
    aide: 'Documentation',
    language: 'Language',
    theme: 'Appearance',
    nbAlbums: 'Number of albums from last listens',
    action: 'Actions',
    actions: '<b>Left click</b>: Play, put on queue or special action<br><b>Middle click or Alt/Option + click</b>: Select (useful for creating a compilation)<br><b>Right click</b>: See more information',
    //panneau sel
    exempleCompil: 'My playlist',
    creerCompil: 'Create a playlist',
    creer: 'Create',
    choisirCouv: 'Choose a cover or drag it here',
    selection: 'Selection',
    erreurImg: 'Image name already present. You have to change it (just below the picture).',
    //panneau mixer
    compilations: 'Playlists',
    dernieresEcoutes: 'Last plays'
}