const cn = {
    creationCollection: '创建音乐收藏。',
    musiquesAnalysees: '音乐分析。',
    pasAlbum: '找不到专辑。 音乐文件夹的路径可以在设置中更改。',
    //panneau recherche
    oeuvreParIndex: '按索引工作',
    rechercheGlobale: '全球搜寻',
    resultats: '结果：',
    recherche: '专辑，作品，艺术家或曲目标题名称。',
    //panneau album
    disque: '碟片',
    //panneau file d'attente
    fileAttenteVide: '队列为空。',
    vider: '空',
    historiqueVide: '历史记录是空的。',
    pisteAttente: (nb, time) => `${nb} + 音乐等待(${time})。`,
    pisteLancee: nb => `${nb > 1 ? '推出'+nb+'种音乐' : '音乐发布'}。`,
    //panneau filtres
    genres: '音乐流派',
    annees: '岁月',
    retirerFiltres: '删除所有过滤器',
    //panneau params
    dossierMusical: '音乐唱片',
    dossierConfig: '配置文件夹',
    choisirDossier: '选择一个音乐文件夹',
    informations: '信息',
    version: 'Musiko版本',
    aide: '文件',
    language: '语言',
    theme: '出现',
    nbAlbums: '最近一次聆听的专辑数量',
    action: '动作',
    actions: '<b>左键</b>：播放，排队或执行特殊操作<br> <b>中键或Alt/Option +单击</b>：选择（用于创建编辑<br><b>右 点击</b>：查看更多信息',
    //panneau sel
    exempleCompil: '我的播放清单',
    creerCompil: '建立播放清单',
    creer: '创建',
    choisirCouv: '选择封面或将其拖到此处',
    selection: '选拔',
    erreurImg: '图片名称已存在。 您必须更改它（仅在图片下方）。',
    //panneau mixer
    compilations: '合编',
    dernieresEcoutes: '最后播放'
}