const AsideSel = {
	props: {app: Object},
	components: {Album, Tracks},
	data: function () {
		return {
			opened: 'selection',
			namePlaylist: this.app.L.exempleCompil,
			over: false,
			coverSrc: false,
			coverName: false,
			coverExt: false,
			errCover: false
		}
	},
	template: `
		<div class=panel>
			<span
				@click=deselectAll
				v-html=$root.I.clear
				class=btClear
			></span>
			<div class="title1 toggle" @click="changeOpened('create playlist')">
				<div
					class="icon noLeft"
					:class="{rotate: this.opened === 'create playlist'}"
					v-html=this.$root.I.fold
				></div>
				<div v-html=app.L.creerCompil></div>	
			</div>
			<div v-if="opened === 'create playlist'" class=padding>
				<input v-model=namePlaylist>
				<input style="display:none;" type=file id=filepicker name=fileList accept="image/png, image/jpeg, image/svg+xml, image/bmp, image/webp" @change=changePath>
				<div v-if=coverSrc class=coverAreaWithCover>
					<div v-html=$root.I.clear @click=resetCover class=clearCover></div>
					<img :src=coverSrc style="width:100%;">
					<input v-model=coverName @input="errCover=false" style="display:inline-block; width:88%; font-size:.9em;"><span style="font-size:.8em;padding-left:4px;">{{coverExt}}</span>
				</div>
				<div v-else
					@dragover=dragover($event)
					@dragleave=dragleave
					@drop=drop($event)
					@click=openChooser
					class=coverArea
					:class="{hover: over}"
					v-html=app.L.choisirCouv
				></div>
				<div v-if=errCover class=errCover v-html=errCover></div>
				<div v-if="namePlaylist.trim() !== '' && coverName && coverName.trim() !== ''" class=bt style="width: 100%" v-html=app.L.creer @click=createPlaylist></div>
			</div>
			<!--
			<div class="title1 toggle" @click="changeOpened('add to a playlist')">
				<div
					class="icon noLeft"
					:class="{rotate: this.opened === 'add to a playlist'}"
					v-html=this.$root.I.fold
				></div>
				<div>Ajouter à une compilation</div>	
			</div>
			-->
			<div v-if="opened === 'add to a playlist'">
				<div v-for="album of app.lists">
					<div class=searchResult @click="">
						<img style="width:22%;margin-right:.5em;" :src=album.cover>
						<div>{{album.album}}</div>
					</div>
				</div>
			</div>
			<div class="title1 toggle" @click="changeOpened('selection')">
				<div
					class="icon noLeft"
					:class="{rotate: this.opened === 'selection'}"
					v-html=this.$root.I.fold
				></div>
				<div>{{app.L.selection}} ({{app.sel.length}}) </div>	
			</div>
			<div v-if="opened === 'selection'">
				<Tracks :app=app :tracks=app.sel :removeIn=app.sel></Tracks>
			</div>
			<br><br>
		</div>
	`,
	methods: {
		resetCover: function() {
			this.coverSrc = false
			this.coverName = false
			this.coverExt = false
		},
		openChooser: function() {
			document.getElementById('filepicker').click()
		},
		changePath: function (e) {
			if (e.target.files.length === 0) return
			this.getCover(e.target.files[0])
		},
		dragover: function(e) {
			this.over = true
			e.preventDefault();
			e.dataTransfer.dropEffect = 'copy';
			return false;
		},
		dragleave: function() {
			this.over = false
			return false
		},
		drop: function(e) {
			this.over = false
			console.log('drop', e)
			e.preventDefault()
			if (e.dataTransfer.files.length === 0) return
			this.getCover(e.dataTransfer.files[0])
			return false
		},
		getCover: function (f) {
			if (['image/png', 'image/jpeg', 'image/svg+xml', 'image/bmp', 'image/webp'].indexOf(f.type)) {
				//on a bien une image
				const ext = path.extname(f.name)
				this.coverSrc = f.path
				this.coverName = path.basename(f.name, ext)
				this.coverExt = ext
			}
		},
		createPlaylist: function() {
			const { COPYFILE_EXCL } = fs.constants
			try {
				if (path.dirname(this.coverSrc) === this.app.collectionPath) {
					//l'image est déjà dans le dossier musical
					// 2 cas se présentent : l'image a le même nom : on ne fait rien, l'image n'a pas le même nom, on la duplique
					const ext = path.extname(this.coverSrc)
					if (this.coverName !== path.basename(this.coverSrc, ext)) {
						fs.copyFileSync(
							this.coverSrc,
							path.join(this.app.collectionPath, this.coverName.trim() + this.coverExt),
							COPYFILE_EXCL
						)
					}
				} else {
					fs.copyFileSync(
						this.coverSrc,
						path.join(this.app.collectionPath, this.coverName.trim() + this.coverExt),
						COPYFILE_EXCL
					)
				}
				let data = ['']
				data.push('#' + this.namePlaylist.trim())
				data.push('>/' + this.coverName.trim() + this.coverExt)
				for (const t of this.app.sel) data.push(t.track.path.normalize())
				data = data.join('\n')
				if (fs.existsSync(path.join(this.app.collectionPath, 'play.lists'))) {
					//on insère la compilation
					fs.appendFileSync(path.join(this.app.collectionPath, 'play.lists'), data)
				} else {
					//on crée le fichier et on y insère la compilation
					fs.writeFileSync('play.lists', data)
				}
				console.log('ipc: envoi de requête')
				ipcRenderer.send('getLists', this.app)
				this.$root.tab = 'Mixer'
				this.app.sel = []
				this.opened = 'selection'
				this.namePlaylist= this.app.L.exempleCompil
				this.over= false
				this.coverSrc= false
				this.coverName= false
				this.coverExt= false
				this.errCover= false
			} catch (err) {
				this.errCover = this.app.L.erreurImg
			}
			
		},
		changeOpened: function(text) {
			if (text === this.opened) {
				this.opened = ''
				return
			}
			this.opened = text
		},
		deselectAll: function() {
			this.app.sel = []
			this.$root.tab = this.app.currentForTriple
		}
	}
}
