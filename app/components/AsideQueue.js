const AsideQueue = {
	props: {app: Object},
	data: function () {
		return {isHover: false}
	},
	components: {Tracks},
	template: `
		<div class=panel>
			<div v-if="app.queue.length === 0" style="text-align:center; padding:20px;">
				<em v-html=app.L.fileAttenteVide></em>
			</div>
			<div v-else>
				<div class=title1>
					{{ nbQueue }}<br>
					<span class=btClear style="font-size: .9em; font-weight: normal; cursor: pointer;" @click=clear :title=app.L.vider v-html=$root.$data.I.clear></span>
				</div>
				<Tracks :app=app :tracks=tracks(app.queue) :removeIn=app.queue></Tracks>
			</div>
			<div v-if="app.past.length === 0" style="text-align:center; padding:20px;">
				<em v-html=app.L.historiqueVide></em>
			</div>
			<div v-else>
				<div class=title1>{{ nbPast }}</div>
				<Tracks :app=app :tracks="tracks(app.past)" :removeIn=app.past></Tracks>
			</div>
			<br><br>
		</div>
	`,
	computed: {
		nbQueue: function () {
			const nb = this.app.queue.length
			let d = 0
			for (let q of this.app.queue) d += q.track.duration
			const time = this.app.getTime(d)
			return this.app.L.pisteAttente(nb, time)
		},
		nbPast: function () {
			const nb = this.app.past.length
			return this.app.L.pisteLancee(nb)
		}
	},
	methods: {
		remove: function(q) {
			this.app.queue.splice(this.app.queue.findIndex(e => e.track.path === q.track.path), 1)
		},
		clear: function() {
			this.app.queue = []
		},
		tracks: function(tracks) {
			return tracks.map(t => ({track: t.track, album: t.album}))
		}
	}
}
