const AsideSearch = {
	props: {app: Object},
	data: function () {
		return {
			fArtworks: [],
			fAlbums: [],
			fArtworksAlbums: [],
			fDiscs: [],
			fArtists: [],
			fTracks : [],
			fArtistsFull: [],
			//
			opened: 'search',
			//
			text: '', //text in input for search
			//
			isSelected: false,
			isSelectedArtist: false
		}
	},
	components: {Albums, Tracks},
	template: `
		<div class=panel>
			<div class="title1 toggle" @click="changeOpened('search')">
				<div class="icon noLeft" :class="{rotate: this.opened === 'search'}" v-html=this.$root.I.fold></div> {{app.L.rechercheGlobale}}
			</div>
			<div v-if="opened === 'search'">
				<div class=padding>
					<p v-html=app.L.recherche></p>
					<div style="display: flex;">
						<input type=text v-focus v-model=text @keyup.enter=searcher>
						<div v-if="text.length > 2" class=bt v-html=$root.I.search @click=searcher></div>
					</div>
					<p v-if="total > 0">{{app.L.resultats}} <b>{{total}}</b></p>
				</div>
				<div v-if="fArtworks.length + fAlbums.length + fArtworksAlbums.length + fDiscs.length > 0" style="margin-top: 1.5em;">
					<div class=title2>{{fArtworks.length + fAlbums.length + fArtworksAlbums.length + fDiscs.length}} <span class=icon v-html=$root.I.album></span></div>
					<Albums :app=app :albums=fArtworks find=artwork></Albums>
					<Albums :app=app :albums=fAlbums find=album></Albums>
					<Albums :app=app :albums=fArtworksAlbums find=both></Albums>
					<div v-if="fDiscs.length > 0">
						<div
							v-for="f of fDiscs"
							@click.exact=rootClick($event,1,f)
							@click.right.exact=rootClick($event,2,f)
							@click.alt.exact=rootClick($event,3,f)
							@click.middle.exact=rootClick($event,3,f)
							class=searchResult
							:class="{selected: isSelected}"
						>
							<img v-if=f.d.disccover style="width:15%;margin-right:.5em;" :src=f.d.disccover>
							<div><b>{{f.d.disctitle}}</b><br>{{f.a.album}}</div>
						</div>
					</div>
				</div>
				<div v-if="fArtists.length > 0" style="margin-top: 1.5em;">
					<div class=title2>{{fArtists.length}} <span class=icon v-html=$root.I.artist></span></div>
					<div
						v-for="f of fArtists"
						@click.exact=rootClick2($event,1,f)
						@click.right.exact=rootClick2($event,2,f)
						@click.alt.exact=rootClick2($event,3,f)
						@click.middle.exact=rootClick2($event,3,f)
						class=searchResult
						:class="{selected: isSelectedArtist}"
					>
						<b>{{f.artist}}</b>
					</div>
				</div>
				<div v-if="fTracks.length > 0" style="margin-top: 1.5em;">
					<div class=title2>{{fTracks.length}} <span class=icon v-html=$root.I.track></span></div>
					<Tracks :app=app :tracks=fTracks :search=true></Tracks>
				</div>
			</div>


			<div class="title1 toggle" @click="changeOpened('genres')">
				<div class="icon noLeft" :class="{rotate: this.opened === 'genres'}" v-html=this.$root.I.fold></div> {{app.L.genres}}
			</div>
			<div v-if="opened === 'genres'" class=tags>
				<span
					v-for="(g, index) of app.genres"
					@click="app.getAlbumsAndTracksOf(g, 'genre')"
				>{{g}}</span>
			</div>
			<div v-if="opened === 'genres' && app.byGenre.albums.length + app.byGenre.tracks.length > 0">
				<div class=title2 v-if="app.byGenre.albums.length > 0">{{app.byGenre.albums.length}} <span class=icon v-html=$root.I.album></span></div>
				<Albums :app=app :albums=app.byGenre.albums find=no></Albums>
				<div class=title2 v-if="app.byGenre.tracks.length > 0">{{app.byGenre.tracks.length}} <span class=icon v-html=$root.I.track></span></div>
				<Tracks :app=app :tracks=app.byGenre.tracks :search=true></Tracks>
			</div>
			<div class="title1 toggle" @click="changeOpened('years')">
				<div class="icon noLeft" :class="{rotate: this.opened === 'years'}" v-html=this.$root.I.fold></div> {{app.L.annees}}
			</div>
			<div v-if="opened === 'years'" class=tags>
				<span
					v-for="(y, index) of app.years"
					@click="app.getAlbumsAndTracksOf(y, 'year')"
				>{{y}}</span>
			</div>
			<div v-if="opened === 'years' && app.byYear.albums.length + app.byYear.tracks.length > 0">
				<div class=title2 v-if="app.byYear.albums.length > 0">{{app.byYear.albums.length}} <span class=icon v-html=$root.I.album></span></div>
				<Albums :app=app :albums=app.byYear.albums find=no></Albums>
				<div class=title2 v-if="app.byYear.tracks.length > 0">{{app.byYear.tracks.length}} <span class=icon v-html=$root.I.track></span></div>
				<Tracks :app=app :tracks=app.byYear.tracks :search=true></Tracks>
			</div>
			<br>
		</div>
	`,
	directives: {
		focus: {inserted: function (el) {el.focus()}}
	},
	computed: {
		total: function () {
			const len = this.fArtworks.length + this.fAlbums.length + this.fArtworksAlbums.length + this.fDiscs.length + this.fArtists.length + this.fTracks.length
			return len
		}
	},
	methods: {
		rootClick: function(e, nb, f) {
			/*
				1: Mettre en attente
				2: Jouer directement
				3: Voir les infos
				4: Afficher le menu contextuel
				5: Sélectionner / Déselectionner
			*/
			const clk = this.app['click' + nb]
			if (clk === 1) this.playDisc(f)
			else if (clk === 2) this.playDirectDisc(f)
			else if (clk === 3) this.showAlbum(f.a)
			else if (clk === 4) this.app.getCtx(e)
			else if (clk === 5) this.toggleSelectedDisc(f)
		},
		rootClick2: function(e, nb, f) {
			const clk = this.app['click' + nb]
			if (clk === 1) this.playArtistTracks(f)
			else if (clk === 2) this.playDirectDisc(f)
			else if (clk === 3) this.showArtist(f)
			else if (clk === 4) this.app.getCtx(e)
			else if (clk === 5) this.toggleSelectedArtist(f)
		},
		changeOpened: function(text) {
			if (text === this.opened) {
				this.opened = ''
				return
			}
			this.opened = text
		},
		showAlbum: function(album) {
			const track = album.discs[0].tracks[0]
			this.app.inPan = {track, album}
			this.$root.tab = 'Album'
		},
		playDisc: function(discAndAlbum) {
			const {d, a} = discAndAlbum
			const index = a.discs.findIndex(disc => d.disctitle === disc.disctitle)
			this.app.playAlbum(a, index)
			this.app.showPause = true
		},
		playDirectDisc: function(discAndAlbum) {
			//const {d, a} = discAndAlbum
		},
		playArtistTracks: function(track) {
			for (const t of this.fArtistsFull) {
				if (t.artist === track.artist) this.app.playTrack({track: t.t, album: t.a})
			}
		},
		toggleSelectedDisc: function(discAndAlbum) {
			const {d, a} = discAndAlbum
			const index = a.discs.findIndex(disc => d.disctitle === disc.disctitle)
			this.app.selectAlbum(this.album, index)
			this.isSelected = true
			setTimeout(function () {
				this.isSelected = false
			}.bind(this), 600)
		},
		showArtist: function(track) {
			this.app.inPan = {track: track.t, album: track.a, artist: [track.artist]}
			this.$root.tab = 'Artist'
		},
		toggleSelectedArtist: function(track) {
			this.app.selectArtists([track.artist])
			this.isSelectedArtist = true
			setTimeout(function () {
				this.isSelectedArtist = false
			}.bind(this), 600)
		},
		searcher: function () {
			this.fArtworks = []
			this.fAlbums = []
			this.fArtworksAlbums = []
			this.fDiscs = []
			this.fArtists = []
			this.fTracks = []
			this.fArtistsFull = []
			const char = this.text.toUpperCase()
			if (char.length < 3) return
			for (const a of this.app.albums) for (const d of a.discs) {
				for (const t of d.tracks) {
					if (a.artwork !== t.artists.join(', ') && a.artwork.toUpperCase().search(char) !== -1) {
						if (this.fArtworks.findIndex(el => el.artwork === a.artwork && el.album === a.album) === -1) this.fArtworks.push(a)
					}
					if (a.album.toUpperCase().search(char) !== -1) {
						if (this.fAlbums.findIndex(el => el.album === a.album) === -1) this.fAlbums.push(a)
					}
					for (const artist of t.artists) {
						if (artist.toUpperCase().search(char) !== -1) {
							if (this.fArtists.findIndex(el => el.artist === artist) === -1) this.fArtists.push({t, a, artist})
							this.fArtistsFull.push({t, a, artist})
						}
					}
					if (t.title.toUpperCase().search(char) !== -1) {
						this.fTracks.push({track: t, album: a})
					}
				}
				if (d.disctitle && d.disctitle.toUpperCase().search(char) !== -1) {
					this.fDiscs.push({d, a})
				}
			}
			let j = 0
			for (let f of this.fArtworks) {
				const i = this.fAlbums.findIndex(el => el.artwork === f.artwork && el.album === f.album)
				if (i !== -1) {
					this.fArtworksAlbums.push(this.fAlbums[i])
					this.fArtworks.splice(j, 1)
					this.fAlbums.splice(i, 1)
				}
				j++
			}
			j = 0
			for (let f of this.fAlbums) {
				const i = this.fArtworks.findIndex(el => el.artwork === f.artwork && el.album === f.album)
				if (i !== -1) {
					this.fArtworksAlbums.push(this.fArtworks[i])
					this.fAlbums.splice(j, 1)
					this.fArtworks.splice(i, 1)
				}
				j++
			}
		}
	}
}
