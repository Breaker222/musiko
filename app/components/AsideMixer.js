const AsideMixer = {
	props: {app: Object},
	components: {Album, Tracks},
	data: function () {
		return {
			opened: 'compil'
		}
	},
	template: `
		<div class=panel>
			<div class="title1 toggle" @click="changeOpened('compil')">
				<div
					class="icon noLeft"
					:class="{rotate: this.opened === 'compil'}"
					v-html="this.$root.I.fold"
				></div>
				<div v-html=app.L.compilations></div>
			</div>
			<div v-if="opened === 'compil'">
				<div class=albumsGrid>
					<Album v-for="album, i of app.lists" :key="'list'+i" :app=app :album=album :concise=true style="margin: 20px 0;"></Album>
				</div>
			</div>

			<!--Compilations aléatoires-->

			<div class="title1 toggle" @click="changeOpened('last')">
				<div
					class="icon noLeft"
					:class="{rotate: this.opened === 'last'}" 
					v-html=this.$root.I.fold
				></div>
				<div v-html=app.L.dernieresEcoutes></div>
			</div>
			<div v-if="opened === 'last'" class=albumsGrid>
				<Album v-for="album, i of app.lastAlbums" :key="'last'+i" :app=app :album=album :concise=true></Album>
			</div>
			<br>
			<br>
		</div>
	`,
	methods: {
		changeOpened: function(text) {
			if (text === this.opened) {
				this.opened = ''
				return
			}
			this.opened = text
		}
	}
}
