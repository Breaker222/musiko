const AsideArtist = {
	props: {app: Object},
	components: {Albums, Tracks},
	template: `
		<div v-if=app.inPan class=panel>
			<div v-if="artists.length > 0">
				<div v-for="a of artists">
					<div class=title1>{{a.artist}}</div>
					<div class=title2 v-if="a.res.albums.length > 0">{{a.res.albums.length}} <span class=icon v-html=$root.I.album></span></div>
					<Albums :app=app :albums=a.res.albums></Albums>
					<div class=title2 v-if="a.res.tracks.length > 0">{{a.res.tracks.length}} <span class=icon v-html=$root.I.track></span></div>
					<Tracks :app=app :tracks=a.res.tracks></Tracks>
				</div>
			</div>
			<div v-else><em>La piste machin truc est sans artiste défini dans les métadonnées.</em></div>
			<br><br>
		</div>
		<div v-else class=panel style="padding: 20px;"><em>{{app.L.pasAlbum}}</em></div>
	`,
	computed: {
		artists: function () {
			const p = this.app.inPan
			let arr = []
			if (p.artist) {
				for (const artist of p.artist) {
					arr.push({
						artist,
						res: this.app.getAlbumsAndTracksOf(artist, 'artist')
					})
				}
				return arr
			}
			if (p.track.artists && p.track.artists.length > 0) {
				for (const artist of p.track.artists) {
					arr.push({
						artist,
						res: this.app.getAlbumsAndTracksOf(artist, 'artist')
					})
				}
			}
			return arr
		}
	},
	methods: {}
}
